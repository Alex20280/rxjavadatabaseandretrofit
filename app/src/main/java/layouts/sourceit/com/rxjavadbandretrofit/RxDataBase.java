package layouts.sourceit.com.rxjavadbandretrofit;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import rx.Subscriber;

public class RxDataBase {

    public static rx.Observable<Cursor> query(
            final ContentResolver cr,
            final Uri uri,
            final String[] projection,
            final String selection,
            final String[] selectionArgs,
            final String sortOrder) {
        return rx.Observable.create(new rx.Observable.OnSubscribe<Cursor>() {
            @Override
            public void call(Subscriber<? super Cursor> observer) {
                if (!observer.isUnsubscribed()) {
                    Cursor cursor = null;
                    try {
                        cursor = cr.query(uri, projection, selection, selectionArgs, sortOrder);
                        if (cursor != null && cursor.getCount() > 0) {
                            while (cursor.moveToNext()) {
                                observer.onNext(cursor);
                            }
                        }
                        observer.onCompleted();
                    } catch (Exception err) {
                        observer.onError(err);

                    } finally {
                        if (cursor != null) cursor.close();
                    }
                }
            }
        });
    }

}